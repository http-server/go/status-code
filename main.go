package main

import (
	"flag"
	"fmt"
	"net/http"
	"strconv"
)

func httpHandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path[1:]

	if i, err := strconv.Atoi(path); err == nil {
		statusMessage := http.StatusText(i)

		if statusMessage != "" {
			w.WriteHeader(i)
			fmt.Fprintf(w, "%v", i)
		} else {
			fmt.Fprintf(w, "Unknown code. Using default: HTTP 200")
		}
	} else {
		fmt.Fprintf(w, "Using default: HTTP 200")
	}
}

func main() {
	port := flag.Int("port", 8080, "port number to be used for web server")
	flag.Parse()

	address := fmt.Sprintf(":%d", *port)

	http.HandleFunc("/", httpHandler)

	if err := http.ListenAndServe(address, nil); err != nil {
		fmt.Println(err)
	}
}