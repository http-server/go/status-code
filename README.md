HTTP status code web server
===


Description
---

This web server returns HTTP status code `###` when `/###` is visited and `###` is a HTTP status code as described in [the documentation](https://golang.org/pkg/net/http/#pkg-constants). Otherwise, HTTP status code 200 is returned.


Usage
---

```sh
# Build executable
go build -o "build/server-http-status" main.go

# Start server on default port (8080)
./build/server-http-status

# Start server on specific port
./build/server-http-status --port=8008
```

Then, in another terminal, run:

```sh
# 200 OK
curl --head localhost:8080/200

# 301 Moved Permanently
curl --head localhost:8080/301

# 404 Not Found
curl --head localhost:8080/404

# 418 I'm a teapot
curl --head localhost:8080/418

# 200 OK
curl --head localhost:8080/NaN
```